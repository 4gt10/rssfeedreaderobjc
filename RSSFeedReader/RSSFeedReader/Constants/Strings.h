//
//  Strings.h
//  RSSFeedReader
//
//  Created by Ariya House on 20.01.16.
//  Copyright © 2016 Artur Chernov. All rights reserved.
//

static NSString * const UnableToParseRSSFeedByURL = @"UnableToParseRSSFeedByURL";
static NSString * const CheckTheSourceAndTryAgain = @"CheckTheSourceAndTryAgain.";
static NSString * const NoDataToParseByURL = @"NoDataToParseByURL";
static NSString * const Search = @"Search";
static NSString * const EnterRSSURL = @"EnterRSSURL";
static NSString * const SourceURL = @"SourceURL";
static NSString * const Load = @"Load";
static NSString * const IncorrectURL = @"IncorrectURL";
static NSString * const Cancel = @"Cancel";
static NSString * const Error = @"Error";
static NSString * const OK = @"OK";
static NSString * const ProbablyInternetConnectionIsLost = @"ProbablyInternetConnectionIsLost";