//
//  NSDate+Utils.m
//  RSSFeedReader
//
//  Created by Ariya House on 20.01.16.
//  Copyright © 2016 Artur Chernov. All rights reserved.
//

#import "NSDate+Utils.h"

@implementation NSDate (Utils)

+ (NSDateFormatter*) formatter: (NSString*) format {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = format;
    return dateFormatter;
}

+ (NSDate*) dateFromRSSString: (NSString*) string {
    return [[NSDate formatter:@"EEE, d MMM yyyy HH:mm:ss ZZZ"] dateFromString:string];
}

- (NSString*) toString: (NSString*) format {
    return [[NSDate formatter:format] stringFromDate:self];
}

- (NSString*) toRSSString {
    return [[NSDate formatter:@"EEE, d MMM yyyy HH:mm:ss ZZZ"] stringFromDate:self];
}

- (BOOL) isGreaterThanDate: (NSDate*) date {
    return [self compare:date] == NSOrderedDescending;
}

- (BOOL) isLessThanDate: (NSDate*) date {
    return [self compare:date] == NSOrderedAscending;
}

- (BOOL) isSameAsDate: (NSDate*) date {
    return [self compare:date] == NSOrderedSame;
}

@end
