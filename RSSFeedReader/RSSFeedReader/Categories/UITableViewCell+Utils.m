//
//  UITableViewCell+Utils.m
//  RSSFeedReader
//
//  Created by Ariya House on 20.01.16.
//  Copyright © 2016 Artur Chernov. All rights reserved.
//

#import "UITableViewCell+Utils.h"

@implementation UITableViewCell (Utils)

+ (NSString*) cellIdentifier {
    return NSStringFromClass(self);
}

@end
