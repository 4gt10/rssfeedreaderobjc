//
//  NSDate+Utils.h
//  RSSFeedReader
//
//  Created by Ariya House on 20.01.16.
//  Copyright © 2016 Artur Chernov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (Utils)

+ (NSDateFormatter*) formatter: (NSString*) format;
+ (NSDate*) dateFromRSSString: (NSString*) string;

- (NSString*) toString: (NSString*) format;
- (NSString*) toRSSString;
- (BOOL) isGreaterThanDate: (NSDate*) date;
- (BOOL) isLessThanDate: (NSDate*) date;
- (BOOL) isSameAsDate: (NSDate*) date;

@end
