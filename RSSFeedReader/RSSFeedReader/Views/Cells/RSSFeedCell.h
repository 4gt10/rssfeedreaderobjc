//
//  RSSFeedCell.h
//  RSSFeedReader
//
//  Created by Ariya House on 20.01.16.
//  Copyright © 2016 Artur Chernov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RSSFeedCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *feedTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *feedDescriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *feedPublicationDateLabel;

@end
