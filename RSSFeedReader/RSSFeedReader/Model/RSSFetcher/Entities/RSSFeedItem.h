//
//  RSSFeedItem.h
//  RSSFeedReader
//
//  Created by Ariya House on 20.01.16.
//  Copyright © 2016 Artur Chernov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RSSFeedItem : NSObject

@property (nonatomic, copy) NSString *link;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *itemDescription;
@property (nonatomic, copy) NSString *pubDateString;
@property (nonatomic, strong) NSDate *pubDate;

@end
