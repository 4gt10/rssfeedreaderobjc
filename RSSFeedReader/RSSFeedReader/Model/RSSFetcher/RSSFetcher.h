//
//  RSSFetcher.h
//  RSSFeedReader
//
//  Created by Ariya House on 20.01.16.
//  Copyright © 2016 Artur Chernov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RSSFeed.h"

typedef void(^onCompletion)(RSSFeed* feed, NSError *error);

@interface RSSFetcher : NSObject

- (void) feedByURL: (NSURL*) URL completion: (onCompletion) completion;

@end
