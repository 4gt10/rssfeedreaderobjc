//
//  RSSFetcher.m
//  RSSFeedReader
//
//  Created by Ariya House on 20.01.16.
//  Copyright © 2016 Artur Chernov. All rights reserved.
//

#import "RSSFetcher.h"
#import "RSSFeedItem.h"
#import "NetworkUtils.h"
#import "Strings.h"
#import "StringsUtils.h"
#import "NSDate+Utils.h"

@interface RSSFetcher() <NSXMLParserDelegate>

@property (nonatomic, strong) NSXMLParser *parser;
@property (nonatomic, copy) NSString *currentElementName;
@property (nonatomic, strong) RSSFeed *currentFeed;
@property (nonatomic, strong) RSSFeedItem *currentItem;
@property (nonatomic, strong) RSSFeed *feed;
@property (nonatomic, copy) onCompletion completion;

@end

@implementation RSSFetcher

#pragma mark - Interface

- (void) feedByURL: (NSURL*) URL completion: (onCompletion) completion {
    self.completion = completion;
    
    self.feed = [[RSSFeed alloc] init];
    self.feed.rssLink = URL.absoluteString;
    self.feed.items = [[NSMutableArray alloc] init];
    
    if ([NetworkUtils isConnectedToNetwork]) {
        // Online
        NSURLRequest *request = [NSURLRequest requestWithURL:URL];
        NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
        NSURLSessionTask *task = [session dataTaskWithRequest:request
                                            completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
                                                if (error) {
                                                    self.completion(nil,error);
                                                } else {
                                                    if (data) {
                                                        [self parseXMLData:data];
                                                        [self saveFeedToFile:data];
                                                    } else {
                                                        NSError *error = [self errorWithHost:URL.host
                                                                                     message:[NSString stringWithFormat:
                                                                                              @"%@:/n%@/n%@",
                                                                                              [StringsUtils stringForKey:NoDataToParseByURL],
                                                                                              URL.absoluteString,
                                                                                              [StringsUtils stringForKey:CheckTheSourceAndTryAgain]
                                                                                              ]
                                                                          ];
                                                        self.completion(nil,error);
                                                    }
                                                }
                                            }
                                  ];
        [task resume];
    } else {
        // Offline
        NSData *data = [self loadFeedFromFile];
        if (data) {
            [self parseXMLData:data];
        } else {
            NSError *error = [self errorWithHost:URL.host
                                         message:[StringsUtils stringForKey:ProbablyInternetConnectionIsLost]
                              ];
            self.completion(nil,error);
        }
    }
}

#pragma mark - NSXMLParserDelegate

NSString * const Channel = @"channel";
NSString * const Item = @"item";
NSString * const Title = @"title";
NSString * const Link = @"link";
NSString * const Description = @"description";
NSString * const PublicationDate = @"pubDate";

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary<NSString *,NSString *> *)attributeDict {
    self.currentElementName = elementName;
    if ([elementName isEqualToString:Channel]) {
        self.currentFeed = [[RSSFeed alloc] init];
    } else if ([elementName isEqualToString:Item]) {
        self.currentItem = [[RSSFeedItem alloc] init];
    }
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {
    NSString *stringWithoutWhitespaceAndNewlineCharacters = [string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    // We are inside "channel" element
    if (self.currentFeed) {
        if ([self.currentElementName isEqualToString:Title]) {
            if (self.currentFeed.title) {
                self.currentFeed.title = [self.currentFeed.title stringByAppendingString:stringWithoutWhitespaceAndNewlineCharacters];
            } else {
                self.currentFeed.title = stringWithoutWhitespaceAndNewlineCharacters;
            }
        } else if ([self.currentElementName isEqualToString:Link]) {
            if (self.currentFeed.link) {
                self.currentFeed.link = [self.currentFeed.link stringByAppendingString:stringWithoutWhitespaceAndNewlineCharacters];
            } else {
                self.currentFeed.link = stringWithoutWhitespaceAndNewlineCharacters;
            }
        }
    // We are inside "item" element
    } else if (self.currentItem) {
        if ([self.currentElementName isEqualToString:Title]) {
            if (self.currentItem.title) {
                self.currentItem.title = [self.currentItem.title stringByAppendingString:stringWithoutWhitespaceAndNewlineCharacters];
            } else {
                self.currentItem.title = stringWithoutWhitespaceAndNewlineCharacters;
            }
        } else if ([self.currentElementName isEqualToString:Description]) {
            if (self.currentItem.itemDescription) {
                self.currentItem.itemDescription = [self.currentItem.itemDescription stringByAppendingString:stringWithoutWhitespaceAndNewlineCharacters];
            } else {
                self.currentItem.itemDescription = stringWithoutWhitespaceAndNewlineCharacters;
            }
        } else if ([self.currentElementName isEqualToString:Link]) {
            if (self.currentItem.link) {
                self.currentItem.link = [self.currentItem.link stringByAppendingString:stringWithoutWhitespaceAndNewlineCharacters];
            } else {
                self.currentItem.link = stringWithoutWhitespaceAndNewlineCharacters;
            }
        } else if ([self.currentElementName isEqualToString:PublicationDate]) {
            if (self.currentItem.pubDateString) {
                self.currentItem.pubDateString = [self.currentItem.pubDateString stringByAppendingString:stringWithoutWhitespaceAndNewlineCharacters];
            } else {
                self.currentItem.pubDateString = stringWithoutWhitespaceAndNewlineCharacters;
            }
        }
    }
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    self.currentElementName = @"";
    if ([elementName isEqualToString:Item]) {
        if (self.currentItem) {
            RSSFeedItem *copy = [[RSSFeedItem alloc] init];
            copy.title = self.currentItem.title;
            copy.itemDescription = self.currentItem.itemDescription;
            copy.link = self.currentItem.link;
            copy.pubDateString = self.currentItem.pubDateString;
            copy.pubDate = [NSDate dateFromRSSString:self.currentItem.pubDateString];
            [self.feed.items addObject:copy];
            self.currentItem = nil;
        }
    // Avoiding reading attributes in an inner channel entity with the same names
    } else if ([elementName isEqualToString:Title] || [elementName isEqualToString:Link]) {
        if (self.currentFeed) {
            if (self.currentFeed.title && ![self.currentFeed.title isEqualToString:@""] &&
                self.currentFeed.link && ![self.currentFeed.link isEqualToString:@""]) {
                self.feed.title = self.currentFeed.title;
                self.feed.link = self.currentFeed.link;
                self.currentFeed = nil;
            }
        }
    }
}

- (void)parserDidEndDocument:(NSXMLParser *)parser {
    self.completion(self.feed,nil);
}

- (void)parser:(NSXMLParser *)parser parseErrorOccurred:(NSError *)parseError {
    self.completion(nil,parseError);
}

- (void)parser:(NSXMLParser *)parser validationErrorOccurred:(NSError *)validationError {
    self.completion(nil,validationError);
}

#pragma mark - Helpers

- (void) parseXMLData: (NSData*) data {
    self.parser = [[NSXMLParser alloc] initWithData:data];
    if (self.parser) {
        self.parser.delegate = self;
        self.parser.shouldResolveExternalEntities = NO;
        [self.parser parse];
    } else {
        NSError *error = [self errorWithHost:self.feed.rssLink
                                     message:[NSString stringWithFormat:
                                              @"%@:/n%@/n%@",
                                              [StringsUtils stringForKey:UnableToParseRSSFeedByURL],
                                              self.feed.rssLink, [StringsUtils stringForKey:CheckTheSourceAndTryAgain]
                                              ]
                          ];
        self.completion(nil,error);
    }
}

- (void) saveFeedToFile:(NSData*) data {
    [data writeToFile:[self getFeedCashFilePath] atomically:YES];
}

- (NSData*) loadFeedFromFile {
    return [NSData dataWithContentsOfFile:[self getFeedCashFilePath]];
}

- (NSString*) getFeedCashFilePath {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = paths.firstObject;
    return [NSString stringWithFormat:@"%@/%lu", documentsDirectory, (unsigned long)self.feed.rssLink.hash];
}

- (NSError*) errorWithHost: (NSString*) host message: (NSString*) message {
    return [NSError errorWithDomain:host
                               code:0
                           userInfo:@{NSLocalizedDescriptionKey : message}
            ];
}

@end
