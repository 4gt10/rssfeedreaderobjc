//
//  RSSFeedViewController.m
//  RSSFeedReader
//
//  Created by Ariya House on 20.01.16.
//  Copyright © 2016 Artur Chernov. All rights reserved.
//

#import "RSSFeedViewController.h"
#import "RSSFeed.h"
#import "RSSFeedItem.h"
#import "RSSFeedCell.h"
#import "RSSFetcher.h"
#import "UITableViewCell+Utils.h"
#import "NSDate+Utils.h"
#import "Strings.h"
#import "StringsUtils.h"

@interface RSSFeedViewController () <UITableViewDataSource, UITableViewDelegate, UIAlertViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *rssTableView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loadingAIW;

@property (nonatomic, strong) RSSFeed *feed;
@property (nonatomic, copy) NSArray *monthsNames;

@end

@implementation RSSFeedViewController

#pragma mark - View controller lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Navigation item
    UIBarButtonItem *changeSourceItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSearch target:self action:@selector(changeSource)];
    UIBarButtonItem *openRSSURLItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction target:self action:@selector(openRSSURL)];
    NSArray *rightBarButtonItems = @[openRSSURLItem, changeSourceItem];
    self.navigationItem.rightBarButtonItems = rightBarButtonItems;
    
    // Refresh control
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventValueChanged];
    [self.rssTableView addSubview:refreshControl];
    
    // Table view cells
    self.rssTableView.rowHeight = UITableViewAutomaticDimension;
    self.rssTableView.estimatedRowHeight = 96;  // IB value
    
    [self loadLastViewedRSS];
}

#pragma mark - Table view

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.monthsNames.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSString *monthName = self.monthsNames[section];
    NSArray *feedItems = [self getFeedByMonthName:monthName];
    return feedItems.count;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return self.monthsNames[section];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    RSSFeedCell *cell = [tableView dequeueReusableCellWithIdentifier:[RSSFeedCell cellIdentifier]];
    RSSFeedItem *feedItem = self.feed.items[indexPath.row];
    
    cell.feedTitleLabel.text = feedItem.title;
    cell.feedDescriptionLabel.text = feedItem.itemDescription;
    cell.feedPublicationDateLabel.text = [feedItem.pubDate toString:@"dd MMMM yyyy, HH:mm"];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    RSSFeedItem *feedItem = self.feed.items[indexPath.row];
    NSURL *URL = [NSURL URLWithString:feedItem.link];
    if (URL) {
        [UIApplication.sharedApplication openURL:URL];
    }
}

#pragma mark - Alert view

const NSInteger OKButtonIndex = 1;

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == OKButtonIndex) {
        UITextField *textField = [alertView textFieldAtIndex:0];
        NSURL *URL = [NSURL URLWithString:textField.text];
        if (URL) {
            [self fetchFeed:URL];
        } else {
            [self showErrorAlertWithMessage:[StringsUtils stringForKey:IncorrectURL]];
        }
    }
}

#pragma mark - Actions

- (void) changeSource {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:[StringsUtils stringForKey:Search]
                                                    message:[StringsUtils stringForKey:EnterRSSURL]
                                                   delegate:self
                                          cancelButtonTitle:[StringsUtils stringForKey:Cancel]
                                          otherButtonTitles:[StringsUtils stringForKey:Load], nil
                          ];
    alert.alertViewStyle = UIAlertViewStylePlainTextInput;
    UITextField *textField = [alert textFieldAtIndex:0];
    textField.placeholder = [StringsUtils stringForKey:SourceURL];
    textField.clearButtonMode = UITextFieldViewModeWhileEditing;
    textField.text = self.feed.rssLink;
    [alert show];
}

- (void) openRSSURL {
    NSURL *URL = [NSURL URLWithString:self.feed.link];
    if (URL) {
        [UIApplication.sharedApplication openURL:URL];
    }
}

- (void)refresh:(UIRefreshControl *)refreshControl {
    [refreshControl endRefreshing];
    [self fetchFeed:[NSURL URLWithString:self.feed.rssLink]];
}

#pragma mark - Helpers

#pragma mark - Fetching/saving/loading data

- (void) fetchFeed: (NSURL*) URL {
    [self startLoadingAnimations];
    RSSFetcher *fetcher = [[RSSFetcher alloc] init];
    [fetcher feedByURL:URL completion:^(RSSFeed *feed, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self stopLoadingAnimations];
            if (error) {
                [self showErrorAlertWithMessage:error.localizedDescription];
            } else {
                self.feed = feed;
                [self sortFeedByDate];
                self.monthsNames = [self getMonthNamesFromFeed];
                
                // Cache
                [self saveLastViewedRSSURL];
                
                // Update UI
                self.title = self.feed.title;
                [self.rssTableView reloadData];
            }
        });
    }];
}

NSString * const LastViewedRSSURLKey = @"Last viewed RSS URL";
NSString * const DefaultRSSURLKey = @"Default RSS URL";

- (void) saveLastViewedRSSURL {
    [[NSUserDefaults standardUserDefaults] setURL:[NSURL URLWithString:self.feed.rssLink] forKey:LastViewedRSSURLKey];
}

- (void) loadLastViewedRSS {
    NSURL *lastViewedRSSURL = [[NSUserDefaults standardUserDefaults] URLForKey:LastViewedRSSURLKey];
    if (lastViewedRSSURL) {
        [self fetchFeed:lastViewedRSSURL];
    } else {
        NSURL *defaultURL = [NSURL URLWithString:[[NSBundle mainBundle] objectForInfoDictionaryKey:DefaultRSSURLKey]];
        [self fetchFeed:defaultURL];
    }
}

#pragma mark - UI

- (void) startLoadingAnimations {
    self.rssTableView.hidden = YES;
    [self.loadingAIW startAnimating];
    UIApplication.sharedApplication.networkActivityIndicatorVisible = YES;
}

- (void) stopLoadingAnimations {
    self.rssTableView.hidden = NO;
    [self.loadingAIW stopAnimating];
    UIApplication.sharedApplication.networkActivityIndicatorVisible = NO;
}

- (void) showErrorAlertWithMessage: (NSString*) message {
    [[[UIAlertView alloc] initWithTitle:[StringsUtils stringForKey:Error]
                               message:message
                              delegate:nil
                     cancelButtonTitle:[StringsUtils stringForKey:OK]
                     otherButtonTitles:nil
      ] show];
}

#pragma mark - Sorting/grouping

- (void) sortFeedByDate {
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"pubDate"
                                                 ascending:NO];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    self.feed.items = [[self.feed.items sortedArrayUsingDescriptors:sortDescriptors] mutableCopy];
}

- (NSArray*) getMonthNamesFromFeed {
    NSMutableArray *monthNames = [NSMutableArray array];
    for (RSSFeedItem* feedItem in self.feed.items) {
        NSString *monthName = [[NSDate formatter:@"MMMM"] stringFromDate:feedItem.pubDate];
        if (![monthNames containsObject:monthName]) {
            [monthNames addObject:monthName];
        }
    }
    return monthNames;
}

- (NSArray*) getFeedByMonthName: (NSString*) monthName {
    NSMutableArray *feedItems = [NSMutableArray array];
    for (RSSFeedItem* feedItem in self.feed.items) {
        NSString *currentMonthName = [[NSDate formatter:@"MMMM"] stringFromDate:feedItem.pubDate];
        if ([currentMonthName isEqualToString:monthName]) {
            [feedItems addObject:feedItem];
        }
    }
    return feedItems;
}

@end
