//
//  StringsUtils.h
//  RSSFeedReader
//
//  Created by Ariya House on 20.01.16.
//  Copyright © 2016 Artur Chernov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface StringsUtils : NSObject

+ (NSString*) stringForKey: (NSString*) key;

@end
