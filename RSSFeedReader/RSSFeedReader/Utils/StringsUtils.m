//
//  StringsUtils.m
//  RSSFeedReader
//
//  Created by Ariya House on 20.01.16.
//  Copyright © 2016 Artur Chernov. All rights reserved.
//

#import "StringsUtils.h"

NSString * const StringsTableName = @"Strings";
NSString * const UnknownString = @"???";

@implementation StringsUtils

+ (NSString*) stringForKey: (NSString*) key {
    NSString *string = [[NSBundle mainBundle] localizedStringForKey:key
                                                              value:UnknownString
                                                              table:StringsTableName
                        ];
    return string;
}

@end